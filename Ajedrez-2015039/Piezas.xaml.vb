﻿Imports System.IO.Directory

Public Class Piezas
    Private _imagen As New Image
    Private _imagenBrush As New ImageBrush
    Private _color As Colores

    Public Property Fill As Colores
        Get
            Return Me._color
        End Get
        Set(value As Colores)
            Me._color = value
            UpdateColor(value)
        End Set
    End Property

    Private Sub UpdateColor(ByVal _color As Colores)
        Select Case _color
            Case Colores.Torre1
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\torre1.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Caballo1
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\caballo1.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Alfil1
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\alfil1.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Reina1
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\reina1.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Rey1
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\rey1.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Peon1
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\peon1.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush

            Case Colores.Torre2
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\torre2.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Caballo2
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\caballo2.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Alfil2
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\alfil2.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Reina2
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\reina2.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Rey2
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\rey2.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
            Case Colores.Peon2
                _imagen.Source = New BitmapImage(New Uri(GetCurrentDirectory().ToString() & "\Recursos\peon2.png"))
                _imagenBrush.ImageSource = _imagen.Source
                Content.Background = _imagenBrush
        End Select
    End Sub
End Class
