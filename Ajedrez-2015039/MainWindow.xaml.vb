﻿Imports System.Timers.Timer

Class MainWindow
    Public _matriz(0 To 7, 0 To 7) As Piezas

    Private temporizador As Timers.Timer
    Private Hora As Integer = 0
    Private Minuto As Integer = 0
    Private Segundo As Integer = 0

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        ColocacionPiezas()
    End Sub

    Private Sub ColocacionPiezas()
        For _y As Integer = 0 To 7
            For _x As Integer = 0 To 7
                If (_x Mod 2) Then
                    Dim blackSquare As New Rectangle
                    blackSquare.Fill = _lightCyan
                    If (_y Mod 2) Then
                        Grid.SetRow(blackSquare, _y)
                        Grid.SetColumn(blackSquare, _x - 1)
                    Else
                        Grid.SetRow(blackSquare, _y)
                        Grid.SetColumn(blackSquare, _x)
                    End If
                    Tablero.Children.Add(blackSquare)
                Else
                    Dim whiteSquare As New Rectangle
                    whiteSquare.Fill = _lightBlue
                    If (_y Mod 2) Then
                        Grid.SetRow(whiteSquare, _y)
                        Grid.SetColumn(whiteSquare, _x + 1)
                    Else
                        Grid.SetRow(whiteSquare, _y)
                        Grid.SetColumn(whiteSquare, _x)
                    End If
                    Tablero.Children.Add(whiteSquare)
                End If
            Next
        Next

        '---Azules 
        For _peonesA As Integer = 0 To 7
            Dim _peon1 As New Piezas
            _peon1.Fill = Colores.Peon1
            Grid.SetColumn(_peon1, _peonesA)
            Grid.SetRow(_peon1, 6)
            Tablero.Children.Add(_peon1)
            _matriz(_peonesA, 6) = _peon1
        Next

        Dim _torreA1 As New Piezas
        _torreA1.Fill = Colores.Torre1
        Grid.SetColumn(_torreA1, 0)
        Grid.SetRow(_torreA1, 7)
        Tablero.Children.Add(_torreA1)
        _matriz(0, 7) = _torreA1

        Dim _torreA2 As New Piezas
        _torreA2.Fill = Colores.Torre1
        Grid.SetColumn(_torreA2, 7)
        Grid.SetRow(_torreA2, 7)
        Tablero.Children.Add(_torreA2)
        _matriz(7, 7) = _torreA2

        Dim _caballoA1 As New Piezas
        _caballoA1.Fill = Colores.Caballo1
        Grid.SetColumn(_caballoA1, 1)
        Grid.SetRow(_caballoA1, 7)
        Tablero.Children.Add(_caballoA1)
        _matriz(1, 7) = _caballoA1

        Dim _caballoA2 As New Piezas
        _caballoA2.Fill = Colores.Caballo1
        Grid.SetColumn(_caballoA2, 6)
        Grid.SetRow(_caballoA2, 7)
        Tablero.Children.Add(_caballoA2)
        _matriz(6, 7) = _caballoA2

        Dim _alfilA1 As New Piezas
        _alfilA1.Fill = Colores.Alfil1
        Grid.SetColumn(_alfilA1, 2)
        Grid.SetRow(_alfilA1, 7)
        Tablero.Children.Add(_alfilA1)
        _matriz(2, 7) = _alfilA1

        Dim _alfilA2 As New Piezas
        _alfilA2.Fill = Colores.Alfil1
        Grid.SetColumn(_alfilA2, 5)
        Grid.SetRow(_alfilA2, 7)
        Tablero.Children.Add(_alfilA2)
        _matriz(5, 7) = _alfilA2

        Dim _reinaA As New Piezas
        _reinaA.Fill = Colores.Reina1
        Grid.SetColumn(_reinaA, 3)
        Grid.SetRow(_reinaA, 7)
        Tablero.Children.Add(_reinaA)
        _matriz(3, 7) = _reinaA

        Dim _reyA As New Piezas
        _reyA.Fill = Colores.Rey1
        Grid.SetColumn(_reyA, 4)
        Grid.SetRow(_reyA, 7)
        Tablero.Children.Add(_reyA)
        _matriz(4, 7) = _reyA

        '----Celestes
        For _peonesC As Integer = 0 To 7
            Dim _peon2 As New Piezas
            _peon2.Fill = Colores.Peon2
            Grid.SetColumn(_peon2, _peonesC)
            Grid.SetRow(_peon2, 1)
            Tablero.Children.Add(_peon2)
            _matriz(_peonesC, 1) = _peon2
        Next

        Dim _torreC1 As New Piezas
        _torreC1.Fill = Colores.Torre2
        Grid.SetColumn(_torreC1, 0)
        Grid.SetRow(_torreC1, 0)
        Tablero.Children.Add(_torreC1)
        _matriz(0, 0) = _torreC1

        Dim _torreC2 As New Piezas
        _torreC2.Fill = Colores.Torre2
        Grid.SetColumn(_torreC2, 7)
        Grid.SetRow(_torreC2, 0)
        Tablero.Children.Add(_torreC2)
        _matriz(7, 0) = _torreC2

        Dim _caballoC1 As New Piezas
        _caballoC1.Fill = Colores.Caballo2
        Grid.SetColumn(_caballoC1, 1)
        Grid.SetRow(_caballoC1, 0)
        Tablero.Children.Add(_caballoC1)
        _matriz(1, 0) = _caballoC1

        Dim _caballoC2 As New Piezas
        _caballoC2.Fill = Colores.Caballo2
        Grid.SetColumn(_caballoC2, 6)
        Grid.SetRow(_caballoC2, 0)
        Tablero.Children.Add(_caballoC2)
        _matriz(6, 0) = _caballoC2

        Dim _alfilC1 As New Piezas
        _alfilC1.Fill = Colores.Alfil2
        Grid.SetColumn(_alfilC1, 2)
        Grid.SetRow(_alfilC1, 0)
        Tablero.Children.Add(_alfilC1)
        _matriz(2, 0) = _alfilC1

        Dim _alfilC2 As New Piezas
        _alfilC2.Fill = Colores.Alfil2
        Grid.SetColumn(_alfilC2, 5)
        Grid.SetRow(_alfilC2, 0)
        Tablero.Children.Add(_alfilC2)
        _matriz(5, 0) = _alfilC2

        Dim _reinaC As New Piezas
        _reinaC.Fill = Colores.Reina2
        Grid.SetColumn(_reinaC, 3)
        Grid.SetRow(_reinaC, 0)
        Tablero.Children.Add(_reinaC)
        _matriz(3, 0) = _reinaC

        Dim _reyC As New Piezas
        _reyC.Fill = Colores.Rey2
        Grid.SetColumn(_reyC, 4)
        Grid.SetRow(_reyC, 0)
        Tablero.Children.Add(_reyC)
        _matriz(4, 0) = _reyC
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles Timer1.Tick
        Segundo += 1
        If Segundo = 59 Then
            Segundo = 0
            Minuto += 1
            If Minuto = 59 Then
                Minuto += 1
                Hora += 1
            End If
        End If
    End Sub

    Private Sub Window_SizeChanged(sender As Object, e As SizeChangedEventArgs)
        tagA.Width = (e.NewSize.Width \ 9)
        tagB.Width = (e.NewSize.Width \ 9)
        tagC.Width = (e.NewSize.Width \ 8)
        tagD.Width = (e.NewSize.Width \ 8)
        tagE.Width = (e.NewSize.Width \ 9)
        tagF.Width = (e.NewSize.Width \ 9)
        tagG.Width = (e.NewSize.Width \ 8)
        tagH.Width = (e.NewSize.Width \ 8)

        tag0.Height = (e.NewSize.Height \ 10)
        tag1.Height = (e.NewSize.Height \ 10)
        tag2.Height = (e.NewSize.Height \ 10)
        tag3.Height = (e.NewSize.Height \ 9)
        tag4.Height = (e.NewSize.Height \ 9)
        tag5.Height = (e.NewSize.Height \ 10)
        tag6.Height = (e.NewSize.Height \ 10)
        tag7.Height = (e.NewSize.Height \ 10)
    End Sub

    Private Sub Timer1()
    End Sub

    Dim _lightBlue As New SolidColorBrush(Colors.LightBlue)
    Dim _lightCyan As New SolidColorBrush(Colors.LightCyan)

    Dim _blackColor As New SolidColorBrush(Colors.Black)
    Dim _whiteColor As New SolidColorBrush(Colors.White)

    Dim _mouseX As Integer
    Dim _mouseY As Double

    Dim _turno As Boolean = True
    Dim _imagen As Piezas
    Dim _x As Integer
    Dim _y As Integer

    Private Sub Tablero_MouseDown(sender As Object, e As MouseButtonEventArgs)
        If (_turno = True) Then
            If (IsNothing(_imagen)) Then
                _imagen = _matriz(_mouseX, _mouseY)
                _x = _mouseX
                _y = _mouseY
                'MsgBox("vacio")
                _turno = True
            ElseIf (_imagen.Fill = Colores.Peon2) Then
                If (_mouseX <> _x) Then
                    'si se mueve a los lados
                    MsgBox("No podes mover a los lados!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = True
                ElseIf (_mouseY = 3 And _mouseY = _y + 2) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("peon2")
                    _imagen = Nothing
                    _turno = False
                ElseIf (_mouseY = _y + 1) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("peon2")
                    _imagen = Nothing
                    _turno = False
                    'ElseIf Decomer Then
                    'ElseIf de Then comer
                End If
            ElseIf (_imagen.Fill = Colores.Torre2) Then
                'VERIFICACION DE PIEZAS CRUZADAS
                For a As Integer = 0 To 7
                    For b As Integer = 0 To 7
                        If (a / b = 1) Then
                            'MsgBox("No podes mover cruzado!")
                            _imagen = _matriz(_mouseX, _mouseY)
                            _x = _mouseX
                            _y = _mouseY
                            _turno = True
                        Else
                            Tablero.Children.Remove(_matriz(_x, _y))
                            _matriz(_x, _y) = Nothing
                            Grid.SetRow(_imagen, _mouseY)
                            Grid.SetColumn(_imagen, _mouseX)
                            Tablero.Children.Add(_imagen)
                            _matriz(_mouseX, _mouseY) = _imagen

                            MsgBox("torre2")
                            _imagen = Nothing
                            _turno = False
                        End If
                    Next
                Next

                'If (_mouseY = _y - 1) Then
                '    MsgBox("No podes mover cruzado!")
                '    _imagen = _matriz(_mouseX, _mouseY)
                '    _x = _mouseX
                '    _y = _mouseY
                '    _turno = True
                'Else
                '    Tablero.Children.Remove(_matriz(_x, _y))
                '    _matriz(_x, _y) = Nothing
                '    Grid.SetRow(_imagen, _mouseY)
                '    Grid.SetColumn(_imagen, _mouseX)
                '    Tablero.Children.Add(_imagen)
                '    _matriz(_mouseX, _mouseY) = _imagen

                '    MsgBox("torre2")
                '    _imagen = Nothing
                '    _turno = False
                'End If
            ElseIf (_imagen.Fill = Colores.Caballo2) Then
                Tablero.Children.Remove(_matriz(_x, _y))
                _matriz(_x, _y) = Nothing
                Grid.SetRow(_imagen, _mouseY)
                Grid.SetColumn(_imagen, _mouseX)
                Tablero.Children.Add(_imagen)
                _matriz(_mouseX, _mouseY) = _imagen

                MsgBox("caballo2")
                _imagen = Nothing
                _turno = False
            ElseIf (_imagen.Fill = Colores.Alfil2) Then
                If (_mouseX = _x + 1 Or _mouseY = _x + 2 Or _mouseX = _x + 3 Or _mouseX = _x + 4 Or _mouseX = _x + 5 Or _mouseX = _x + 6 Or _mouseX = _x + 7) And
                    (_mouseY = _y - 1 Or _mouseY = _y - 2 Or _mouseY = _y - 3 Or _mouseY = _y - 4 Or _mouseY = _y - 5 Or _mouseY = _y - 6 Or _mouseY = _y - 7) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("alfil2")
                    _imagen = Nothing
                    _turno = False
                    'ElseIf (_mouseX = _x - 1 Or _mouseY = _x - 2 Or _mouseX = _x - 3 Or _mouseX = _x - 4 Or _mouseX = _x - 5 Or _mouseX = _x - 6 Or _mouseX = _x - 7) Or
                    ' (_mouseY = _y + 1 Or _mouseY = _y + 2 Or _mouseY = _y + 3 Or _mouseY = _y + 4 Or _mouseY = _y + 5 Or _mouseY = _y + 6 Or _mouseY = _y + 7) Then
                    '    Tablero.Children.Remove(_matriz(_x, _y))
                    '    _matriz(_x, _y) = Nothing
                    '    Grid.SetRow(_imagen, _mouseY)
                    '    Grid.SetColumn(_imagen, _mouseX)
                    '    Tablero.Children.Add(_imagen)
                    '    _matriz(_mouseX, _mouseY) = _imagen

                    '    MsgBox("alfil2")
                    '    _imagen = Nothing
                    '    _turno = False
                Else
                    MsgBox("No puedes mover en lineas rectas!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = True
                End If
            ElseIf (_imagen.Fill = Colores.Reina2) Then
                Tablero.Children.Remove(_matriz(_x, _y))
                _matriz(_x, _y) = Nothing
                Grid.SetRow(_imagen, _mouseY)
                Grid.SetColumn(_imagen, _mouseX)
                Tablero.Children.Add(_imagen)
                _matriz(_mouseX, _mouseY) = _imagen

                MsgBox("reina2")
                _imagen = Nothing
                _turno = False
            ElseIf (_imagen.Fill = Colores.Rey2) Then
                Tablero.Children.Remove(_matriz(_x, _y))
                _matriz(_x, _y) = Nothing
                Grid.SetRow(_imagen, _mouseY)
                Grid.SetColumn(_imagen, _mouseX)
                Tablero.Children.Add(_imagen)
                _matriz(_mouseX, _mouseY) = _imagen

                MsgBox("rey2")
                _imagen = Nothing
                _turno = False
            Else
                If (_imagen.Fill = Colores.Peon1) Then
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    MsgBox("Peon, no es turno azul!")
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Torre1) Then
                    MsgBox("Torre, no es turno azul!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Caballo1) Then
                    MsgBox("Caballo, no es turno azul!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Alfil1) Then
                    MsgBox("Alfil, no es turno azul!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Reina1) Then
                    MsgBox("Reina, no es turno azul!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Rey1) Then
                    MsgBox("Rey, no es turno azul!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = True
                End If
            End If
        ElseIf (_turno = False) Then
            If (IsNothing(_imagen)) Then
                _x = _mouseX
                _y = _mouseY
                _imagen = _matriz(_mouseX, _mouseY)
                'MsgBox("Vacio")
                _turno = False
            ElseIf (_imagen.Fill = Colores.Peon1) Then
                If (_mouseX <> _x) Then
                    'si se mueve a los lados
                    MsgBox("No podes mover a los lados!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = False
                ElseIf (_mouseY = 4 And _mouseY = _y - 2) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("peon1")
                    _imagen = Nothing
                    _turno = True
                ElseIf (_mouseY = _y - 1) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("peon1")
                    _imagen = Nothing
                    _turno = True
                    'ElseIf Decomer Then
                    'ElseIf de Then comer
                End If



            ElseIf (_imagen.Fill = Colores.Torre1) Then
                If (_mouseY = _y + 1) Then
                    MsgBox("No podes mover cruzado!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = False
                Else
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("torre1")
                    _imagen = Nothing
                    _turno = True
                End If

            ElseIf (_imagen.Fill = Colores.Caballo1) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("caballo1")
                    _imagen = Nothing
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Alfil1) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("alfil1")
                    _imagen = Nothing
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Reina1) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("reina1")
                    _imagen = Nothing
                    _turno = True
                ElseIf (_imagen.Fill = Colores.Rey1) Then
                    Tablero.Children.Remove(_matriz(_x, _y))
                    _matriz(_x, _y) = Nothing
                    Grid.SetRow(_imagen, _mouseY)
                    Grid.SetColumn(_imagen, _mouseX)
                    Tablero.Children.Add(_imagen)
                    _matriz(_mouseX, _mouseY) = _imagen

                    MsgBox("rey1")
                    _imagen = Nothing
                    _turno = True
                Else
                    If (_imagen.Fill = Colores.Peon2) Then
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    MsgBox("Peon, no es turno celeste!")
                    _turno = False
                ElseIf (_imagen.Fill = Colores.Torre2) Then
                    MsgBox("Torre, no es turno celeste!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = False
                ElseIf (_imagen.Fill = Colores.Caballo2) Then
                    MsgBox("Caballo, no es turno celeste!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = False
                ElseIf (_imagen.Fill = Colores.Alfil2) Then
                    MsgBox("Alfil, no es turno celeste!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = False
                ElseIf (_imagen.Fill = Colores.Reina2) Then
                    MsgBox("Reina, no es turno celeste!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = False
                ElseIf (_imagen.Fill = Colores.Rey2) Then
                    MsgBox("Rey, no es turno celeste!")
                    _imagen = _matriz(_mouseX, _mouseY)
                    _x = _mouseX
                    _y = _mouseY
                    _turno = False
                End If
            End If
        End If
    End Sub

    Dim _letras As String = "abcdefgh"
    Dim _numeros As String = "12345678"
    Dim _mouseX1 As String
    Dim _mouseY1 As String

    Private Sub Tablero_MouseMove(sender As Object, e As MouseEventArgs)
        _mouseX = e.GetPosition(sender).X \ 75
        _mouseX1 = Convert.ToString(_mouseX).Replace(0, _letras(0)).Replace("1", _letras(1)).Replace(2, _letras(2)).Replace(3, _letras(3)).
            Replace(4, _letras(4)).Replace(5, _letras(5)).Replace(6, _letras(6)).Replace(7, _letras(7))

        _mouseY = e.GetPosition(sender).Y \ 75
        _mouseY1 = Convert.ToString(_mouseY).Replace(7, _numeros(7)).Replace(6, _numeros(6)).Replace(5, _numeros(5)).Replace(4, _numeros(4)).
            Replace(3, _numeros(3)).Replace(2, _numeros(2)).Replace(1, _numeros(1)).Replace(0, _numeros(0))
        Informacion.Text = "Posición: " & _mouseX1 & "," & _mouseY1 & "; Pieza: " & "-----" & "; Gana: -----; Turno: " & CStr(_turno).Replace(True, "Celestes").Replace(False, "Azules") & "; Tiempo: " & Hora & ":" & Minuto & ":" & Segundo & "."
    End Sub

    Private Sub nuevoJuego_Click(sender As Object, e As RoutedEventArgs)
        Tablero.Children.Clear()
        For a = 0 To 7
            For b = 0 To 7
                _matriz(a, b) = Nothing
            Next
        Next
        ColocacionPiezas()
        _turno = True
    End Sub

    Private Sub salir_Click(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub

    Private Sub Window_KeyDown(sender As Object, e As KeyEventArgs)
        If ((e.Key = Key.F1) Or (Keyboard.Modifiers = ModifierKeys.Control And e.Key = Key.N)) Then
            Tablero.Children.Clear()
            For a = 0 To 7
                For b = 0 To 7
                    _matriz(a, b) = Nothing
                Next
            Next
            ColocacionPiezas()
            _turno = True
        ElseIf ((e.Key = Key.F2) Or (Keyboard.Modifiers = ModifierKeys.Control And e.Key = Key.s)) Then
            Close()
        End If
    End Sub
End Class
