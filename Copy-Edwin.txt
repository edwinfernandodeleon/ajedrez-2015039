        If (_alternador = True) Then
            If (IsNothing(_objeto)) Then
                _objeto = _matriz(_mouseX, _mouseY)
                _newX = _mouseX
                _newY = _mouseY
            ElseIf Not (IsNothing(_objeto)) Then
                If Not (_objeto.Fill = Colores.Peon) Then
                    _objeto = Nothing
                Else
                    Tablero.Children.Remove(_matriz(_newX, _newY))
                    _matriz(_newX, _newY) = Nothing
                    Grid.SetRow(_objeto, _mouseY)
                    Grid.SetColumn(_objeto, _mouseX)
                    Tablero.Children.Add(_objeto)
                    _matriz(_mouseX, _mouseY) = _objeto
                    _objeto = Nothing
                    _alternador = False
                End If
            End If
        End If